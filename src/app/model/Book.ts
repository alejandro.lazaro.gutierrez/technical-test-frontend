export class Book
{
  id!:string;
  title!:string;
  subtitle!:string;
  isbn!:string;
  publisherName!:string;
  publishedYear!:number;
  price!:number;
}
