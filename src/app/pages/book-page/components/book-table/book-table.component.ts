import { debounceTime } from 'rxjs/operators';
import { Book } from '../../../../model/Book';
import { PaginateResponseDTO } from './../../../../dtos/PaginateResponseDTO';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ConfirmEventType, LazyLoadEvent, MessageService } from 'primeng/api';
import {ConfirmationService} from 'primeng/api';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-book-table',
  templateUrl: './book-table.component.html',
  styleUrls: ['./book-table.component.css'],
  providers: [ConfirmationService, MessageService]
})
export class BookTableComponent implements OnInit {

  @Input() books!: Book[];
  @Input() totalBooks!: number;
  @Input() loading:boolean = true;

  @Output() pagination =  new EventEmitter<{page: number, size: number}>();
  @Output() updateEvent = new EventEmitter<Book>();
  @Output() removeEvent = new EventEmitter<string>();
  @Output() searchEvent = new EventEmitter<any>();

  searchFields: any = {};

  searchFieldsSubject = new Subject();

  constructor(private confirmationService: ConfirmationService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.searchFieldsSubject.pipe(debounceTime(1000)).subscribe(data=>{
      this.searchEvent.emit(data);
    });
  }

  loadBooks(event: LazyLoadEvent){
    this.loading = true;
    let page = (event.first || 0) / (event.rows || 1);
    this.pagination.emit({page, size: event.rows || 10});
  }

  updateBook(book: Book){
    this.updateEvent.emit(book);
  }

  deleteBook(book: Book){
    this.confirmationService.confirm({
        message: `Do you want to delete <strong>${book.title.toUpperCase()}</strong>?`,
        header: 'Delete Confirmation',
        icon: 'pi pi-info-circle',
        accept: () => {
            this.removeEvent.emit(book.id);
        },
        reject: (type: any) => {
            switch(type) {
                case ConfirmEventType.REJECT:
                    this.messageService.add({severity:'error', summary:'Rejected', detail:'You have rejected'});
                break;
                case ConfirmEventType.CANCEL:
                    this.messageService.add({severity:'warn', summary:'Cancelled', detail:'You have cancelled'});
                break;
            }
        },
        key: "positionDialog"
    });
  }

  search(event: any, field: string){
    if(event.target.value !== ""){
      this.searchFields[field] = event.target.value;
    } else{
      delete this.searchFields[field];
    }
    this.searchFieldsSubject.next(this.searchFields);
  }

}
