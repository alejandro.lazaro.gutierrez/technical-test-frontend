import { Book } from './../../../../model/Book';
import { BookForCreationDTO } from './../../../../dtos/BookForCreationDTO';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {

  @Output() saveBookEvent = new EventEmitter<BookForCreationDTO>();

  @Input() set book(inputBook: Book){
    console.log(inputBook)
    if(inputBook.id !== undefined){
      this.bookForm.setValue(inputBook);
    }
  }


  bookForm = new FormGroup({
    id: new FormControl(null),
    title: new FormControl('',[Validators.required]),
    subtitle: new FormControl(''),
    isbn: new FormControl('',[Validators.required]),
    publisherName: new FormControl('',[Validators.required]),
    publishedYear: new FormControl(0,[Validators.required]),
    price: new FormControl(0,[Validators.required, Validators.min(0)]),
  });

  constructor() { }

  ngOnInit(): void {
  }

  get title(){
    return this.bookForm.get('title');
  }

  get subtitle(){
    return this.bookForm.get('subtitle');
  }

  get isbn(){
    return this.bookForm.get('isbn');
  }

  get publisherName(){
    return this.bookForm.get('publisherName');
  }

  get publishedYear(){
    return this.bookForm.get('publishedYear');
  }

  get price(){
    return this.bookForm.get('price');
  }


  save(){

    if (!this.bookForm.valid) {
      Object.values(this.bookForm.controls).forEach(control => {
        if (!control.valid) {
          control.markAsTouched();
        }
      });
      return;
    }


    if(this.bookForm.valid){
      this.saveBookEvent.emit(this.bookForm.value as BookForCreationDTO);
      this.resetForm();
    }
  }

  resetForm(){
    this.bookForm.reset();
    this.book = new Book();
  }
}
