import { MessageService } from 'primeng/api';
import { Book } from '../../model/Book';
import { PaginateResponseDTO } from '../../dtos/PaginateResponseDTO';
import { BookService } from './../../services/book.service';
import { Component, OnInit } from '@angular/core';

import {  debounce, debounceTime } from "rxjs/operators";
import { interval } from 'rxjs';

@Component({
  selector: 'app-book-page',
  templateUrl: './book-page.component.html',
  styleUrls: ['./book-page.component.css'],
  providers: [MessageService]
})
export class BookPageComponent implements OnInit {

  books: Book[] = [];
  totalBooks = 0;
  loadingBooks = true;
  book: Book = new Book();
  searchFieldString = "";

  constructor(private bookService:BookService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.loadBooks(0,10);
  }

  selectNewPage(event: {page: number, size: number}){
    this.loadBooks(event.page, event.size);
  }

  loadBooks(page: number, size: number){
    this.loadingBooks = true;
    this.bookService.getAllBooks(page, size, this.searchFieldString).subscribe((data:PaginateResponseDTO<Book[]>)=> {
      this.books = data.content;
      this.totalBooks = data.total;
      this.loadingBooks = false;
    });
  }

  saveBook(book: Book | any){

    let bookToSave = book as Book;
    console.log(bookToSave)
    if(bookToSave.id === null){
      this.bookService.createBook(bookToSave).subscribe(data=>{
        if(this.totalBooks <= 10)
        {
          this.loadBooks(0,10)
        }
        this.messageService.add({severity:'success', summary:'Confirmed', detail:`Book ${data.title} has been registered`});
      }, ()=>{
        this.messageService.add({severity:'error', summary:'Rejected', detail:'Can\'t register book'});
      });
    } else {
      this.bookService.updateBook(bookToSave).subscribe(data=>{

        this.books.map( book =>{
          if(book.id === bookToSave.id){
            book.id = data.id;
            book.title = data.title;
            book.subtitle = data.subtitle;
            book.isbn = data.isbn;
            book.publisherName = data.publisherName;
            book.publishedYear = data.publishedYear;
            book.price = data.price;
          }
        });

        this.messageService.add({severity:'success', summary:'Confirmed', detail:'Book has been updated'});
      },()=>{
        this.messageService.add({severity:'error', summary:'Rejected', detail:'Can\'t update book'});
      });
    }
  }

  updateEvent(event: any){
    this.book = {...event};
  }

  deleteEvent(bookId: any){
    this.bookService.deleteBook(bookId).subscribe( () =>{
      this.messageService.add({severity:'success', summary:'Confirmed', detail:'Book has been deleted'});
      this.books = this.books.filter( book => book.id !== bookId);
    },()=>{
      this.messageService.add({severity:'error', summary:'Rejected', detail:'Can\'t delete book'});
    });
  }

  searchEvent(searchFields: any){
    this.searchFieldString = "";

    for( const [key, value] of Object.entries(searchFields)){
      this.searchFieldString += `&${key}=${value}`;
    }

    this.loadBooks(0, 10);
  }

}
