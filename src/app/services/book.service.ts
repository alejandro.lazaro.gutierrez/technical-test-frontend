import { Book } from './../model/Book';
import { BookForCreationDTO } from './../dtos/BookForCreationDTO';
import { PaginateResponseDTO } from './../dtos/PaginateResponseDTO';
import { environment } from './../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http: HttpClient) { }

  getAllBooks(
    page: number,
    size: number,
    searchFields: string = ""): Observable<PaginateResponseDTO<Book[]>>{
    return this.http.get<PaginateResponseDTO<Book[]>>(
      `${environment.api}book?page=${page}&size=${size}${searchFields}`
      );
  }

  createBook(book: BookForCreationDTO): Observable<Book>{
    return this.http.post<Book>(`${environment.api}book`, book);
  }

  updateBook(book: any) {
    return this.http.put<Book>(`${environment.api}book`, book);
  }

  deleteBook(id: string){
    return this.http.delete(`${environment.api}book/${id}`)
  }
}
