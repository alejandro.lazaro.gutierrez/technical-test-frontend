import { BookForCreationDTO } from './../dtos/BookForCreationDTO';
import { Book } from './../model/Book';
import { HttpClientTestingModule, HttpTestingController  } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { environment } from 'src/environments/environment.prod';

import { BookService } from './book.service';

describe('BookService', () => {
  let service: BookService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BookService]
    });
    service = TestBed.inject(BookService);
    httpMock =  TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getAllBooks should make a GET request',()=>{
    service.getAllBooks(0,10).subscribe(data=>{});
    httpMock.expectOne({ method: 'GET', url:`${environment.api}book?page=0&size=10`});
  });

  it('createBook should make a POST request',()=>{
    let book = new BookForCreationDTO();

    book.title = "Divina Comedia";
    book.subtitle = "bestseller";
    book.isbn = "121511212";
    book.publisherName = "omega";
    book.publishedYear = 2015;
    book.price = 10.32;


    service.createBook(book).subscribe(data=>{});
    httpMock.expectOne({ method: 'POST', url:`${environment.api}book`});
  });

  it('updateBook should make a PUT request',()=>{
    let book = new Book();

    book.id = "db5c084b-2567-4e03-99b8-2df6b2e55999";
    book.title = "Divina Comedia";
    book.subtitle = "bestseller";
    book.isbn = "121511212";
    book.publisherName = "omega";
    book.publishedYear = 2015;
    book.price = 10.32;


    service.updateBook(book).subscribe(data=>{});
    httpMock.expectOne({ method: 'PUT', url:`${environment.api}book`});
  });

  it('deleteBook should make a DELETE request',()=>{


    let id = "db5c084b-2567-4e03-99b8-2df6b2e55999"

    service.deleteBook(id).subscribe(data=>{});
    httpMock.expectOne({ method: 'DELETE', url:`${environment.api}book/${id}`});
  });
});
