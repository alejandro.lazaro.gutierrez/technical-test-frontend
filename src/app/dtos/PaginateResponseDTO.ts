export class PaginateResponseDTO<T>
{
  content!:T;
  total!: number;
}
