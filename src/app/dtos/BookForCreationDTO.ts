export class BookForCreationDTO
{
  title!:string;
  subtitle!:string;
  isbn!:string;
  publisherName!:string;
  publishedYear!:number;
  price!:number;
}
